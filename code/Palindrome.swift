func isPalindrome(_ text: String) -> Bool {

if ( String(text.reversed()) == text ) {
    return true;
}

else {
    return false;
}

}

for string in ["kayak", "koala"] {
  let isStringAPalindrome = isPalindrome(string)
  print("'\(string)' is \(isStringAPalindrome ? "" : "not ")a palindrome")
}

